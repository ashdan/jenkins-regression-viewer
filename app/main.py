import os
import jenkins
from flask import Flask, render_template
from collections import defaultdict

app = Flask(__name__)

JENKINS_HOST = os.getenv("JENKINS_HOST")
JENKINS_PORT = os.getenv("JENKINS_PORT")
JENKINS_USER = os.getenv("JENKINS_USER")
JENKINS_PASS = os.getenv("JENKINS_PASS")

jenkins_server = jenkins.Jenkins("{}:{}".format(JENKINS_HOST, JENKINS_PORT), username=JENKINS_USER, password=JENKINS_PASS)


def get_all_jobs_builds_tests():
   output = {}
   jobs = jenkins_server.get_all_jobs()
   for job in jobs:
      output[job["name"]] = {}
      builds = jenkins_server.get_job_info(job["name"])
      job_tests_names = []
      for build in builds["builds"]:
         build_info = jenkins_server.get_build_info(job["name"], build["number"])
         test_report = jenkins_server.get_build_test_report(job["name"], build["number"])
         if test_report is not None:
            extracted_tests = extract_test_results(test_report)
            output[job["name"]][build_info["number"]] = extracted_tests
            job_tests_names = extracted_tests.keys()
         else:
            output[job["name"]][build_info["number"]] = None

      # Populate N/A Tests results in case that there was not test results for the build
      if len(job_tests_names) > 0:
         for build_number, tests in output[job["name"]].items():
            if tests is None:
               tests_results = defaultdict(list)
               for test_name in job_tests_names:
                  tests_results[test_name].append("N/A")
               output[job["name"]][build_number] = tests_results

   return output


def extract_test_results(test_report):
   test_mapper = lambda x: {x["name"]: x["status"]}

   tests = list(map(test_mapper, test_report["suites"][0]["cases"]))

   tests_dict = {}
   for test in tests:
      tests_dict.update(test)
   tests_results = defaultdict(list)
   for key, value in tests_dict.items():
      tests_results[key].append(value)
   
   return tests_results


def build_dashboard_data(jobs_data):
   dashboards_data = {}
   for job_name, builds in jobs_data.items():
      sorted_builds_numbers = list(builds.keys())[::-1]
      dashboards_data[job_name] = [
         [None] + sorted_builds_numbers
      ]
      tests_results = defaultdict(list)
      sorted_builds = list(builds.items())[::-1]
      for build_number, tests in sorted_builds:
         if tests is not None:
            for key, value in tests.items():
               tests_results[key].append(value[0])
               
      for test_name, results in tests_results.items():
         dashboards_data[job_name].append([test_name] + results)

   return dashboards_data

@app.route('/')
def dashboard():
   jobs_data = get_all_jobs_builds_tests()
   dashboard_data = build_dashboard_data(jobs_data)
   return render_template('index.html', dashboard_data=dashboard_data)
