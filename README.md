# Jenkins Jobs Tests Dashboard

A basic uswgi-nginx-flask application that uses python-jenkins to connect to jenkins server and pull jobs inforamtion
to display the tests results of each build for a job

# Requierments
- python 3.5 and above
- a running container/server of jenkins e.g. sparkbeyond/jenkins-challenge:0.0.1

# Run in development
- you can use vscode to run in debug mode with the launcher `Jenkins Dashboard`
- or you can run in terminal from the `app` directory as follow:
```sh
# Change the env variables according to your local jenkins settings
FLASK_APP=main.py FLASK_ENV=development JENKINS_HOST=http://${JENKINS_HOST} JENKINS_PORT=${JENKINS_PORT} JENKINS_USER=${JENKINS_USER} JENKINS_PASS=${JENKINS_PASS} python -m flask run --host 0.0.0.0 --port 5000
```
- visit `http://localhost:5000

# Build docker image
- form the main repo path
```sh
docker build . -t jenkins-dashboard
```

# Run in docker container
- Create a `.env` file with your jenkins server info as shown below
```
# Change the env variables according to your local jenkins settings
JENKINS_HOST=http://172.17.0.2 
JENKINS_PORT=8080
JENKINS_USER=admin
JENKINS_PASS=admin
```
- Then run the following
```sh
docker run --rm --name jenkins-dashboard -p5400:80 --env-file=.env jenkins-dashboard
```
- visit `http://localhost:5400
