FROM tiangolo/uwsgi-nginx-flask:python3.5

COPY docker-requirements.txt /opt/requirements.txt
RUN pip install -r /opt/requirements.txt

COPY ./app /app
